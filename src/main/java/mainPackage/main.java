package mainPackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale.Category;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.codehaus.plexus.archiver.jar.Manifest.ExistingSection;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import object.Producto;
import utilsPackage.identifier;
import utilsPackage.utillsClass;

public class main {
	static long startTime = System.currentTimeMillis();
	public static int vecesAvanzado=0;
	private static ArrayList<Producto> productos = new ArrayList();
	private static ArrayList<Producto> productosEnBBDD=new ArrayList();
	private static int ultimaColumna;
	private static int ultimaFila;
	private static ArrayList<String> wasVisitedUrl=new ArrayList<String>();
	private static int excededTime=0;
	private static String currentCategoryLink;
	private static ArrayList<String> categoryParents=new ArrayList<String>();
	private static boolean moreResults=false;
	
	public static void main(String[] args) throws InterruptedException {
    	String timeStamp ;
    	
    	Date now = new Date();
    	System.out.println("Empieza ejecucion : "+now.toString());
		File f = new File(identifier.baseFile);
		if(!(f.exists() && !f.isDirectory())) { 
		   utillsClass.createFile();
		}
		System.out.println("buscando columna por favor espere");
        ultimaColumna=utillsClass.buscarUltimaColumna();
		System.out.println("buscando fila por favor espere");
        ultimaFila=utillsClass.buscarUltimaFila();
        System.out.println("encontradas, cargando Base de datos");
        productosEnBBDD=utillsClass.ReadProducts(ultimaColumna);
        System.out.println("Start index");
        //System.setProperty("webdriver.gecko.driver","webDriver/geckodriver.exe");

		System.setProperty("webdriver.chrome.driver","webDriver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
        WebDriver driver = new ChromeDriver(options);
		//WebDriver driver = new FirefoxDriver();

        driver.navigate().to("https://tiendas.mediamarkt.es/productos");
        //categoryParents.add("https://tiendas.mediamarkt.es/productos");
    	recursividadCategoria(driver);
    	now = new Date();
    	System.out.println("Fin ejecucion : "+now.toString());
        driver.close();
        
	}
	
	public static void recursividadCategoria(WebDriver driver) throws InterruptedException{
		//categoryParents.add(driver.getCurrentUrl());
		if(utillsClass.existsElementXPath(identifier.mainIdentificatorCategory, driver)){//CON SUBCATEGORIA
			currentCategoryLink=driver.getCurrentUrl();
			List<WebElement> subCategorias=null;
			subCategorias=utillsClass.getCategorias(driver,identifier.mainIdentificatorCategory,identifier.firstElement, identifier.lastElement);
			for(int f=0;f<subCategorias.size();f++){
				if(utillsClass.findVisitedLink(wasVisitedUrl, subCategorias.get(f).getAttribute("href"))){//si no lo ha encontrado, entra
					System.out.println("HREF NUEVO: "+subCategorias.get(f).getAttribute("href"));
					wasVisitedUrl.add(subCategorias.get(f).getAttribute("href"));
					try {
						utillsClass.getToCategory(driver, subCategorias.get(f).getAttribute("href"));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					recursividadCategoria(driver);
					subCategorias=utillsClass.getCategorias(driver,identifier.mainIdentificatorCategory,identifier.firstElement, identifier.lastElement);
				}else{
					System.out.println("HREF REPETIDO: "+subCategorias.get(f).getAttribute("href"));
				}
				
			}
			
			
		}else{//SIN SUBCATEGORIA, coger precios e ir hacia atras las veces que haga falta
			//PONER 50 PRODUCTOS
			try{
				if(utillsClass.isMoreThan30(driver)){
					((JavascriptExecutor)driver).executeScript("changePerPagesAjax(0, 50)");
					moreResults=true;
				}
			}catch(Exception e){
				System.out.println("No se ha podido ejecutar javascript, ya que no existe paginacion en esta url: "+driver.getCurrentUrl());
			}
			
			excededTime=0;
    		while(utillsClass.existsElementClass(identifier.lagger, driver)){
    			//System.out.println("existe carga, espera 500ms");
    			if(excededTime>=10000){
    				System.out.println("Carga excedida, recargando");
    				driver.navigate().refresh();
    				excededTime=0;
    			}
    			try {
    				excededTime+=500;
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
			recursividadProductos(driver);
			
		}
		/*
		System.out.println("\nARBOL: ");
		for(int f=0;f<categoryParents.size();f++){
			System.out.println("\t"+categoryParents.get(f));
		}
		
		categoryParents.remove(categoryParents.size()-1);
		
		driver.get(categoryParents.get(categoryParents.size()-1));
		*/
		if(moreResults);{
			driver.navigate().back();
			moreResults=false;
		}
		driver.navigate().back();
		//((JavascriptExecutor)driver).executeScript("window.history.go(-1)");
		
		
		
	}
	
	public static void recursividadProductos(WebDriver driver){
		List <WebElement> prices = driver.findElements(By.xpath(identifier.productPrice));
        List <WebElement> product = driver.findElements(By.xpath(identifier.productTittle));
        long startTime=0;
        long stopTime =0;
        long elapsedTime=0;
        for(int f=0;f<(product.size()>prices.size()?prices.size():product.size());f++){
        	startTime = System.currentTimeMillis();

        	Producto p=new Producto(product.get(f),prices.get(f));
        	int c=0;
        	for(c=0;(c<productosEnBBDD.size())&&!(p.getHref().equals(productosEnBBDD.get(c).getHref()));c++);
        	
        	if(c==productosEnBBDD.size()){//Si ha recorrido todo el array, es que no existe ese producto
            	p.setFila(ultimaFila);
            	utillsClass.guardarProductoNuevo(ultimaColumna,ultimaFila++, p);
            	productosEnBBDD.add(p);
        	}else{
        		p.setFila(productosEnBBDD.get(c).getFila());
    			utillsClass.actualizarPrecioProducto(ultimaColumna, p);
        	}
        	
        	productos.add(p);
        	stopTime = System.currentTimeMillis();
        	elapsedTime = stopTime - startTime;
        	//System.out.println("tiempo tarea: "+elapsedTime);
        	
    			
        	
        }
        
        if(utillsClass.existsPagination(driver)){
        	if(utillsClass.existsElementXPath(identifier.nextPage, driver)){
        		//System.out.println(driver.findElement(By.xpath("(//A[@class='button bPager gray left arrow'])[2]")).getAttribute("onclick"));
        		String string = driver.findElement(By.xpath(identifier.nextPage)).getAttribute("onclick");
        		String[] parts = string.split(";");
        		
        		((JavascriptExecutor)driver).executeScript(parts[0]+";");
        		excededTime=0;
        		while(utillsClass.existsElementClass(identifier.lagger, driver)){
        			//System.out.println("existe carga, espera 500ms");
        			if(excededTime>=10000){
        				System.out.println("Carga excedida, recargando");
        				driver.navigate().refresh();
        				excededTime=0;
        			}
        			try {
        				excededTime+=500;
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
        		recursividadProductos(driver);
        		//driver.navigate().back();
        		try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        		((JavascriptExecutor)driver).executeScript("window.history.go(-1)");
        	}
        }
	}
	
	
	
    	
    	
    

}
