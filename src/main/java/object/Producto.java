package object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilsPackage.identifier;

public class Producto {
	String href;
	String name;
	String price;
	int fila;
	
	public Producto(){
		
	}
	
	public Producto(WebElement toExtract){
		System.out.println(toExtract.getAttribute("innerHTML"));
		this.href=toExtract.findElement(By.xpath(identifier.productTittle)).getAttribute("href");
		this.name=toExtract.findElement(By.xpath(identifier.productTittle)).findElement(By.tagName("span")).getAttribute("innerHTML");
		try{//POR SI PRODUCTO DESCAGALOGADO Y NO TIENE PRECIO
			this.price=toExtract.findElement(By.xpath(identifier.productPrice)).findElement(By.tagName("span")).getAttribute("innerHTML");
		}catch (Exception e){
			
		}
		//this.mostrar();
	}
	
	public Producto(WebElement tittle, WebElement price){
		this.href=tittle.getAttribute("href");
		this.name=tittle.findElement(By.tagName("span")).getAttribute("innerHTML");
		try{//POR SI PRODUCTO DESCAGALOGADO Y NO TIENE PRECIO
			this.price=price.findElement(By.className("bigpricesrc")).getAttribute("innerHTML");
		}catch(Exception e){
			System.out.println("error Producto: "+e);
			this.price="0";
		}
		//this.mostrar();
	}
	
	public Producto(String href,String name, String price, int fila){
		this.href=href;
		this.name=name;
		this.price=price;
		this.fila=fila;
	}
	
	
	
	
	public void mostrar(){
		System.out.println("---------------");
		System.out.println("|HREF: "+ this.href);
		System.out.println("|NAME: "+ this.name);
		System.out.println("|PRICE: "+ this.price);
		System.out.println("---------------");
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getFila() {
		return fila;
	}

	public void setFila(int fila) {
		this.fila = fila;
	}
	
	
	
	
}
