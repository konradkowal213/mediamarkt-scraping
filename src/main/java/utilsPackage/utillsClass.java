package utilsPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import object.Producto;

public class utillsClass {

	private static int salto=0;
	
	private static OPCPackage pkg ;
	
	private static XSSFWorkbook workbook;
	
	private static XSSFSheet sheet;
	
	private static Row row;
	
	private static FileOutputStream outFile;
	
	public static List<WebElement> getCategorias(WebDriver driver,String search1, String search2, String search3 ) throws InterruptedException{
		List<WebElement> subCategorias=null;
		try{
			subCategorias=driver.findElements(By.xpath(search1));
			subCategorias.add(driver.findElement(By.xpath(search2)));
			subCategorias.add(driver.findElement(By.xpath(search3)));
		}catch (Exception e){
			System.out.println("Fuera de servidor, recargando y probando de nuevo...");
			driver.navigate().refresh();
			Thread.sleep(1000);
			subCategorias=getCategorias(driver, search1, search2, search3);
		}
		return subCategorias;
		
    }
	
	public static void createFile(){
		 //Create blank workbook
	      XSSFWorkbook workbook = new XSSFWorkbook(); 

	      //Create a blank sheet
	      XSSFSheet spreadsheet = workbook.createSheet(identifier.sheetName);

	      //Create row object
	      XSSFRow row;

	      //This data needs to be written (Object[])
	      Map < String, Object[] > empinfo = 
	      new TreeMap < String, Object[] >();
	      empinfo.put( "1", new Object[] { "Link", "Name"});
	      
	      //Iterate over data and write to sheet
	      Set < String > keyid = empinfo.keySet();
	      int rowid = 0;

	      for (String key : keyid) {
	         row = spreadsheet.createRow(rowid++);
	         Object [] objectArr = empinfo.get(key);
	         int cellid = 0;

	         for (Object obj : objectArr) {
	            Cell cell = row.createCell(cellid++);
	            cell.setCellValue((String)obj);
	         }
	      }

	      //Write the workbook in file system
	      FileOutputStream out;
		try {
			out = new FileOutputStream(new File(identifier.baseFile));
			workbook.write(out);
		    out.close();
		    System.out.println("new File");     
		} catch (FileNotFoundException e) {
			System.out.println("error 6:"+e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("error 7:"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void getToCategory(WebDriver driver,String categoria) throws InterruptedException{
		driver.navigate().to(categoria);
	}
	
	public static boolean existsElement(String id, WebDriver driver) {
        try {
            driver.findElement(By.id(id));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }
    
    public static boolean existsElementXPath(String id, WebDriver driver) {
    	try {
    		driver.findElement(By.xpath(id));
    	} catch (NoSuchElementException e) {
    		return false;
        }
    	return true;
   	}
    	    		
    public static boolean existsElementClass(String id, WebDriver driver) {
        try {
            driver.findElement(By.className(id));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }
    
    public static boolean existsPagination(WebDriver driver){
    	try{
    		driver.findElement(By.xpath(identifier.paginationExists)).getAttribute("innerHTML").equals("\n");
    	} catch (NoSuchElementException e) {
            return false;
        }
    	return true;
    }

    public static Producto findProducts(int fila,int ultimaColumna) {
    	XSSFWorkbook wb;
    	Sheet sheet;
    	FileInputStream fis ;
    	Row row = null;
    	Cell cell;
    	try {
            fis = new FileInputStream(identifier.baseFile);
            wb = new XSSFWorkbook(fis);
            sheet= wb.getSheet(identifier.sheetName);
            row = sheet.getRow(fila);
            cell = row.getCell(0);
            if (cell==null){
                return null;
            }
            Producto p = new Producto(row.getCell(0).getStringCellValue(),row.getCell(1).getStringCellValue(),"0", fila);
            
            return p;

       } catch (Exception e){
           return null;
       }
    }
    
    public static ArrayList<Producto> ReadProducts(int ultimaColumna) {
    	ArrayList<Producto> productos = new ArrayList() ;
    	int f=1;
    	Producto p;
    	while ((p=findProducts(f,ultimaColumna))!=null){
            productos.add(p);
            f++;
        }
    	System.out.println("Leidos "+productos.size()+" productos de la BBDD");
    	return productos;
    }
    
    public static int buscaColumna(int columna){
    	XSSFWorkbook wb;
    	Sheet sheet;
    	FileInputStream fis ;
    	Row row;
    	Cell cell;
    	try {
            fis = new FileInputStream(identifier.baseFile);
            wb = new XSSFWorkbook(fis);
            sheet= wb.getSheet(identifier.sheetName);
            row = sheet.getRow(0);
            cell = row.getCell(columna);
            if (cell==null){
                return columna;
            }            
            return 0;

       } catch (Exception e){
    	   System.out.println("error 4:"+e);
    	   return 0;
       }
    }

    public static int buscaFila(int fila){
    	XSSFWorkbook wb;
    	Sheet sheet;
    	FileInputStream fis ;
    	Row row;
    	Cell cell;
    	try {
            fis = new FileInputStream(identifier.baseFile);
            wb = new XSSFWorkbook(fis);
            sheet= wb.getSheet(identifier.sheetName);
            row = sheet.getRow(fila);
            cell = row.getCell(0);
            if (cell.getStringCellValue()!=null){
                return 1;
            }            
            return 0;

       } catch (Exception e){
           return 0;
       }
    }
    
    public static int buscarUltimaColumna(){
    	int c=0;
    	int ultimaColumna=0;
    	String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    	while ((ultimaColumna=buscaColumna(c))==0){
    		c++;
    	}
    	
    	try {
    		
    		OPCPackage pkg = OPCPackage.open(new File(identifier.baseFile));
    		XSSFWorkbook  workbook = new XSSFWorkbook (pkg);
	        XSSFSheet sheet = workbook.getSheetAt(0);
	        Row row = sheet.getRow(0);
            row.createCell(ultimaColumna).setCellValue(timeStamp);
           
            
            CellStyle headerCellStyle = workbook.createCellStyle();
            
            
            row.getCell(0).setCellStyle(headerCellStyle);
            
            
            
 	        FileOutputStream outFile =new FileOutputStream(new File(identifier.finalFile));
            workbook.write(outFile);
            
            outFile.close();
            
            pkg.close();
	        
    	} catch (FileNotFoundException e1) {
			System.out.println("error 1:"+e1);
			e1.printStackTrace();
		} catch (IOException e) {
			System.out.println("error 2:"+e);
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			System.out.println("error 3:"+e);
			e.printStackTrace();
		}
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	return ultimaColumna;
    }

    public static void actualizarPrecioProducto2(int columna, Producto p){
    	
    	String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    	
    	try {
    		
    		pkg = OPCPackage.open(new File(identifier.baseFile));
    		workbook = new XSSFWorkbook (pkg);
	        sheet = workbook.getSheetAt(0);
	       
	        Row row = sheet.getRow(p.getFila());
	       
	        row.createCell(columna).setCellValue(p.getPrice());
           
	        FileOutputStream outFile =new FileOutputStream(new File(identifier.finalFile));
            workbook.write(outFile);
            
            outFile.close();
            
            pkg.close();
	        
    	} catch (FileNotFoundException e1) {
			System.out.println("error 9:"+e1);

		} catch (IOException e) {
			System.out.println("error 8:"+e);

		} catch (InvalidFormatException e) {
			System.out.println("error 10:"+e);
			
		}
    	
    	
         
    	 
    }

    public static void actualizarPrecioProducto(int columna, Producto p){
    	if(salto==0){
    		createWorkBook();
    	}else if(salto>=50){
    		writeAndCloseWorkBook();
    		createWorkBook();
    		salto=0;
    	}
    	row = sheet.getRow(p.getFila());
        row.createCell(columna).setCellValue(p.getPrice());
        salto++;
        
    }
    
    public static int buscarUltimaFila(){
    	int f=0;
    	int ultimaFila=0;
    	while ((ultimaFila=buscaFila(f))==1){
    		f++;
    	}
    	return f;
    	
    }

    public static void createWorkBook(){
		try {
			pkg = OPCPackage.open(new File(identifier.baseFile));
			workbook = new XSSFWorkbook (pkg);
			sheet = workbook.getSheetAt(0);
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			System.out.println("error 20:"+e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("error 21:"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static void guardarProductoNuevo(int columnaPrecio, int ultimaFila,Producto p){
    	if(salto==0){
    		createWorkBook();
    	}else if(salto>=10){
    		writeAndCloseWorkBook();
    		createWorkBook();
    		salto=0;
    	}
    	sheet.createRow(ultimaFila);
 	    row = sheet.getRow(ultimaFila);
        row.createCell(0).setCellValue(p.getHref());
        row.createCell(1).setCellValue(p.getName());
        row.createCell(columnaPrecio).setCellValue(p.getPrice());
        salto++;
    }
    
    public static void writeAndCloseWorkBook(){
    	
		try {
			outFile = new FileOutputStream(new File(identifier.finalFile));
			workbook.write(outFile);
	        outFile.close();
	        pkg.close();
		} catch (FileNotFoundException e) {

			System.out.println("error 22:"+e);
			e.printStackTrace();
		} catch (IOException e) {

			System.out.println("error 23:"+e);
			e.printStackTrace();
		}
         
    }
    
    public static void guardarProductoNuevo2(int columnaPrecio, int ultimaFila,Producto p){
    	
    	try {

    		OPCPackage pkg = OPCPackage.open(new File(identifier.baseFile));
    		XSSFWorkbook  workbook = new XSSFWorkbook (pkg);
	        XSSFSheet sheet = workbook.getSheetAt(0);
	        
	        sheet.createRow(ultimaFila);
	        Row row = sheet.getRow(ultimaFila);
            row.createCell(0).setCellValue(p.getHref());
            row.createCell(1).setCellValue(p.getName());
            row.createCell(columnaPrecio).setCellValue(p.getPrice());
            
	        FileOutputStream outFile =new FileOutputStream(new File(identifier.finalFile));
            workbook.write(outFile);
            
            outFile.close();
            
            pkg.close();
	        
    	} catch (FileNotFoundException e1) {
    		System.out.println("error 11:"+e1);
			e1.printStackTrace();
		} catch (IOException e) {
			System.out.println("error 12:"+e);
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			System.out.println("error 13:"+e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	 
         
    	 
    }

    public static boolean findVisitedLink(ArrayList <String> wasVisited,String toVisit){
    	int f;
    	
    	
    	for(f=0;f<wasVisited.size();f++){
    		if(wasVisited.get(f).equals(toVisit)==true){
    			return false;
    		}
    	}
    	
    	/*for(f=0;f<wasVisited.size() && !;f++)
    	if(f==wasVisited.size()){//si la ha llegado al final, es que no lo ha encontrado
    		return false;
    	}*/
    	return true;
    }

    public static boolean isMoreThan30(WebDriver driver){
    	String value=driver.findElement(By.xpath("//DIV[@class='resumePaginator']")).getAttribute("innerText");
    	String[] parts = value.split("\\s+");
    	if(Integer.parseInt(parts[4])>30){
    		return true;
    	}else{
    		return false;
    	}
    	
    }
}
