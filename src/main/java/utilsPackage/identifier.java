package utilsPackage;

public class identifier {
	public static String mainIdentificatorCategory="//A[@class='categoryTree categoryTree1']";
	public static String firstIdentificatorSubCategory="//A[@class='categoryTree categoryTree1 categoryTreeFirst categoryTree1First']";
	public static String lastElement="//A[@class='categoryTree categoryTree1 categoryTreeLast categoryTree1Last']";
	public static String firstElement="//A[@class='categoryTree categoryTree1 categoryTreeFirst categoryTree1First']";
	public static String productTittle="(//A[@class='productName product10Name'])";
	public static String productPrice="(//DIV[@class='productPrices'])";
	public static String productElement="//DIV[@id='categoryProductContainer']";
	public static String paginationExists="(//DIV[@class='pagerContainer'])[2]";
	public static String lagger="MM_layer_container";
	public static String nextPage="(//A[@class='button bPager gray left arrow'])[2]";
	public static String baseFile="excel/NO_MODIFICAR.xlsx";
	public static String finalFile="excel/salida.xlsx";
	public static String sheetName="Precios";
	
}
